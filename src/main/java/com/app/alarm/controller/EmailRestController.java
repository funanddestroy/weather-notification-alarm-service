package com.app.alarm.controller;

import com.app.alarm.dao.impl.EmailRecipientsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/emails")
public class EmailRestController {

    @Autowired
    @Qualifier("emailRecipientsRepository")
    private EmailRecipientsRepository emailRecipientsRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<String> getAll() {
        return emailRecipientsRepository.getRecipientList();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody String email) {
        emailRecipientsRepository.addRecipient(email);
    }

    @RequestMapping(method = RequestMethod.DELETE, value="{email}")
    public void delete(@PathVariable String email) {
        emailRecipientsRepository.deleteRecipient(email);
    }

}
