package com.app.alarm.controller;

import com.app.alarm.dao.impl.SlackRecipientsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/slack-channels")
public class SlackRestController {

    @Autowired
    @Qualifier("slackRecipientsRepository")
    private SlackRecipientsRepository slackRecipientsRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<String> getAll() {
        return slackRecipientsRepository.getRecipientList();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody String channel) {
        slackRecipientsRepository.addRecipient(channel);
    }

    @RequestMapping(method = RequestMethod.DELETE, value="{channel}")
    public void delete(@PathVariable String channel) {
        slackRecipientsRepository.deleteRecipient(channel);
    }

}
