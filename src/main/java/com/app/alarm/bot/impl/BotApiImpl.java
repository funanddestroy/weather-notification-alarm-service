package com.app.alarm.bot.impl;

import com.app.alarm.bot.BotApi;
import com.app.alarm.client.MessageClientList;
import com.app.alarm.client.DatabaseMeasurementClient;
import com.app.alarm.client.MessageClient;
import com.app.alarm.model.Measure;
import com.app.alarm.policy.AlarmPolicy;
import com.app.alarm.service.AlarmPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BotApiImpl implements BotApi {

    @Autowired
    private DatabaseMeasurementClient databaseMeasurementClient;

    @Autowired
    private AlarmPolicyService alarmPolicyService;

    @MessageClientList
    private List<MessageClient> messageClients;


    @Override
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void checkWeather() {

        Measure measure = databaseMeasurementClient.getMeasure();
        for (MessageClient messageClient : messageClients) {
            for (AlarmPolicy alarmPolicy : alarmPolicyService.getAlarmPolicies()) {
                switch (alarmPolicy.getMeasurementName()) {
                    case "temperature":
                        checkParameter(measure.getTemperature(), alarmPolicy, messageClient);
                        break;
                    case "humidity":
                        checkParameter(measure.getHumidity(), alarmPolicy, messageClient);
                        break;
                    case "pressure":
                        checkParameter(measure.getPressure(), alarmPolicy, messageClient);
                        break;
                    case "wind-speed":
                        checkParameter(measure.getWindSpeed(), alarmPolicy, messageClient);
                        break;
                }
            }
        }
    }

    private void checkParameter(int parameter, AlarmPolicy alarmPolicy, MessageClient messageClient) {
        if (parameter > alarmPolicy.getMax()) {
            messageClient.sendMessages(alarmPolicy.getMaxMessage());
        }

        if (parameter < alarmPolicy.getMin()) {
            messageClient.sendMessages(alarmPolicy.getMinMessage());
        }
    }
}
