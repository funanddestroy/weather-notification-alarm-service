package com.app.alarm.client;

import com.app.alarm.model.Measure;

public interface DatabaseMeasurementClient {
    Measure getMeasure();
}
