package com.app.alarm.client;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageClientListAnnotationBeanPostProcessor implements BeanPostProcessor {

    @Autowired
    private ApplicationContext context;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            MessageClientList annotation = field.getAnnotation(MessageClientList.class);
            if (annotation != null) {

                Map<String, MessageClient> beansList = context.getBeansOfType(MessageClient.class);
                List<MessageClient> messageClientList = new ArrayList<>(beansList.values());

                field.setAccessible(true);
                ReflectionUtils.setField(field, bean, messageClientList);
            }
        }
        return bean;
    }
}
