package com.app.alarm.client.impl;

import com.app.alarm.client.MessageClient;
import com.app.alarm.dao.RecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;

public class EmailMessageClient implements MessageClient {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    @Qualifier("emailRecipientsRepository")
    private RecipientRepository emailRecipientRepository;

    private void sendMessage(String email, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Alarm!");
        message.setText(text);
        mailSender.send(message);
    }

    @Override
    public void sendMessages(String text) {
        emailRecipientRepository.getRecipientList().forEach(email -> sendMessage(email, text));
    }
}
