package com.app.alarm.client.impl;

import com.app.alarm.client.MessageClient;
import com.app.alarm.dao.RecipientRepository;
import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.IOException;

public class SlackMessageClient implements MessageClient {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private @Value("${slackBot.token}") String slackBotToken;

    private SlackSession session;

    @Autowired
    @Qualifier("slackRecipientsRepository")
    private RecipientRepository slackRecipientsRepository;

    @PostConstruct
    public void postConstruct() {
        session = SlackSessionFactory.createWebSocketSlackSession(slackBotToken);

        try {
            session.connect();
        } catch (IOException e) {
            logger.error("Could not connect slack session", e);
        }
    }

    @Override
    public void sendMessages(String text) {
        if (session.isConnected()) {
            for (String channel : slackRecipientsRepository.getRecipientList()) {
                SlackChannel slackChannel = session.findChannelByName(channel);
                session.sendMessage(slackChannel, text);
            }
        }
    }
}
