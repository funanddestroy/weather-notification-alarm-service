package com.app.alarm.client.impl;

import com.app.alarm.client.DatabaseMeasurementClient;
import com.app.alarm.model.Measure;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

public class InfluxDatabaseMeasurementClient implements DatabaseMeasurementClient {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private @Value("${database.url}") String url;
    private @Value("${database.username}") String username;
    private @Value("${database.password}") String password;
    private @Value("${database.database}") String database;

    @Override
    public Measure getMeasure() {
        InfluxDB influxDB = InfluxDBFactory.connect(url, username, password);
        Pong response = influxDB.ping();
        if (response.getVersion().equalsIgnoreCase("unknown")) {
            logger.error("Error pinging server");
            return null;
        }

        QueryResult queryResult = influxDB
                .query(new Query("Select * from measure group by * order by desc limit 1", database));

        InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
        List<Measure> measureList = resultMapper
                .toPOJO(queryResult, Measure.class);

        return measureList.get(0);
    }
}
