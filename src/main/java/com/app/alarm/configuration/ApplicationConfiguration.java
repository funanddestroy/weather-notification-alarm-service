package com.app.alarm.configuration;

import com.app.alarm.client.MessageClientListAnnotationBeanPostProcessor;
import com.app.alarm.client.impl.EmailMessageClient;
import com.app.alarm.client.DatabaseMeasurementClient;
import com.app.alarm.client.MessageClient;
import com.app.alarm.client.impl.InfluxDatabaseMeasurementClient;
import com.app.alarm.client.impl.SlackMessageClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class ApplicationConfiguration {

    private @Value("${spring.mail.username}") String mailUsername;
    private @Value("${spring.mail.password}") String mailPassword;

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername(mailUsername);
        mailSender.setPassword(mailPassword);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Bean
    public BeanPostProcessor beanPostProcessor() {
        return new MessageClientListAnnotationBeanPostProcessor();
    }

    @Bean
    public DatabaseMeasurementClient databaseMeasurementClient() {
        return new InfluxDatabaseMeasurementClient();
    }

    @Bean(name = "emailMessageClient")
    public MessageClient emailMessageClient() {
        return new EmailMessageClient();
    }

    @Bean(name = "slackMessageClient")
    public MessageClient slackMessageClient() {
        return new SlackMessageClient();
    }
}
