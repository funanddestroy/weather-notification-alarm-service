package com.app.alarm.policy;

public class AlarmPolicy {
    private String measurementName;
    private int min;
    private int max;
    private String minMessage;
    private String maxMessage;

    public AlarmPolicy(){}

    public AlarmPolicy(String measurementName, int min, int max, String minMessage, String maxMessage) {
        this.measurementName = measurementName;
        this.min = min;
        this.max = max;
        this.minMessage = minMessage;
        this.maxMessage = maxMessage;
    }

    public String getMeasurementName() {
        return measurementName;
    }

    public void setMeasurementName(String measurementName) {
        this.measurementName = measurementName;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getMinMessage() {
        return minMessage;
    }

    public void setMinMessage(String minMessage) {
        this.minMessage = minMessage;
    }

    public String getMaxMessage() {
        return maxMessage;
    }

    public void setMaxMessage(String maxMessage) {
        this.maxMessage = maxMessage;
    }
}
