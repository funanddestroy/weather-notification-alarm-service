package com.app.alarm.service;

import com.app.alarm.policy.AlarmPolicy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@PropertySource("classpath:alarm-policy.properties")
public class AlarmPolicyService {

    private @Value("${alarm.policy.temperature.min:#{NULL}}") Integer temperatureMin;
    private @Value("${alarm.policy.temperature.max:#{NULL}}") Integer temperatureMax;
    private @Value("${alarm.policy.temperature.min-message:#{NULL}}") String temperatureMinMessage;
    private @Value("${alarm.policy.temperature.max-message:#{NULL}}") String temperatureMaxMessage;

    private @Value("${alarm.policy.humidity.min:#{NULL}}") Integer humidityMin;
    private @Value("${alarm.policy.humidity.max:#{NULL}}") Integer humidityMax;
    private @Value("${alarm.policy.humidity.min-message:#{NULL}}") String humidityMinMessage;
    private @Value("${alarm.policy.humidity.max-message:#{NULL}}") String humidityMaxMessage;

    private @Value("${alarm.policy.pressure.min:#{NULL}}") Integer pressureMin;
    private @Value("${alarm.policy.pressure.max:#{NULL}}") Integer pressureMax;
    private @Value("${alarm.policy.pressure.min-message:#{NULL}}") String pressureMinMessage;
    private @Value("${alarm.policy.pressure.max-message:#{NULL}}") String pressureMaxMessage;

    private @Value("${alarm.policy.wind-speed.min:#{NULL}}") Integer windSpeedMin;
    private @Value("${alarm.policy.wind-speed.max:#{NULL}}") Integer windSpeedMax;
    private @Value("${alarm.policy.wind-speed.min-message:#{NULL}}") String windSpeedMinMessage;
    private @Value("${alarm.policy.wind-speed.max-message:#{NULL}}") String windSpeedMaxMessage;

    private List<AlarmPolicy> alarmPolicies;

    public AlarmPolicyService() {
        alarmPolicies = new ArrayList<>();
    }

    @PostConstruct
    public void postConstruct() {

        if (temperatureMin != null && temperatureMax != null &&
                temperatureMinMessage != null && temperatureMaxMessage != null) {
            alarmPolicies.add(new AlarmPolicy("temperature",
                    temperatureMin, temperatureMax, temperatureMinMessage, temperatureMaxMessage));
        }

        if (humidityMin != null && humidityMax != null &&
                humidityMinMessage != null && humidityMaxMessage != null) {
            alarmPolicies.add(new AlarmPolicy("humidity",
                    humidityMin, humidityMax, humidityMinMessage, humidityMaxMessage));
        }

        if (pressureMin != null && pressureMax != null &&
                pressureMinMessage != null && pressureMaxMessage != null) {
            alarmPolicies.add(new AlarmPolicy("pressure",
                    pressureMin, pressureMax, pressureMinMessage, pressureMaxMessage));
        }

        if (windSpeedMin != null && windSpeedMax != null &&
                windSpeedMinMessage != null && windSpeedMaxMessage != null) {
            alarmPolicies.add(new AlarmPolicy("wind-speed",
                    windSpeedMin, windSpeedMax, windSpeedMinMessage, windSpeedMaxMessage));
        }
    }

    public List<AlarmPolicy> getAlarmPolicies() {
        return alarmPolicies;
    }
}
