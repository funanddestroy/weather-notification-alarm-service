package com.app.alarm.dao;

import java.util.List;

public interface RecipientRepository {

    void addRecipient(String recipient);

    void deleteRecipient(String recipient);

    List<String> getRecipientList();
}
