package com.app.alarm.dao.impl;

import com.app.alarm.dao.RecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("slackRecipientsRepository")
public class SlackRecipientsRepository implements RecipientRepository {

    @Autowired
    protected JdbcOperations jdbcOperations;

    @Override
    public void addRecipient(String channel) {

        jdbcOperations.update("INSERT INTO slack_channel VALUES ('" + channel + "');");
    }

    @Override
    public void deleteRecipient(String channel) {

        jdbcOperations.update("DELETE FROM slack_channel WHERE channel = '" + channel + "';");
    }

    @Override
    public List<String> getRecipientList() {

        List<String> result = new ArrayList<>();

        SqlRowSet rowSet = jdbcOperations.queryForRowSet("SELECT * FROM slack_channel;");
        while (rowSet.next()) {
            result.add(rowSet.getString(1));
        }

        return result;
    }
}
