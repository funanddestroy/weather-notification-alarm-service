package com.app.alarm.dao.impl;

import com.app.alarm.dao.RecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Repository("emailRecipientsRepository")
public class EmailRecipientsRepository implements RecipientRepository {

    @Autowired
    protected JdbcOperations jdbcOperations;

    @Override
    public void addRecipient(String email) {

        jdbcOperations.update("INSERT INTO email VALUES ('" + email + "');");
    }

    @Override
    public void deleteRecipient(String email) {

        jdbcOperations.update("DELETE FROM email WHERE email = '" + email + "';");
    }

    @Override
    public List<String> getRecipientList() {

        List<String> result = new ArrayList<>();

        SqlRowSet rowSet = jdbcOperations.queryForRowSet("SELECT * FROM email;");
        while (rowSet.next()) {
            result.add(rowSet.getString(1));
        }

        return result;
    }
}
